# ics-ans-ifc14xx-config

Ansible playbook to setup a tftp server for ifc14xx initial configuration.

## Variables

The following variables are defined at the group level:

```yaml
ifc14xx_config_base_url: http://artifactory.esss.lu.se/artifactory
ifc14xx_config_files:
  - ifc14xx_config/uboot_clear_variables.img
  - ifc14xx_config/uboot_update_env_and_backup.img
  - ifc14xx_config/uboot_update_uboot.img
  - ifc14xx_config/uboot_update_pon_fbis.img
ifc14xx_config_uboot_version: v0.0.23
ifc14xx_config_uboot: boot-images/ioxos/{{ ifc14xx_config_uboot_version }}/u-boot-ifc14xx-64b.bin
ifc14xx_config_fbis_pon_version: 0.0.1
ifc14xx_config_fbis_pon: boot-images/ioxos/fbis_pon/{{ ifc14xx_config_fbis_pon_version }}/pon_microcode_fbis.bin
```

The `ifc14xx_config_uboot_version` can be overriden at the host level in CSEntry
(it can't be overriden in the inventory at the group level).

## License

BSD 2-clause
