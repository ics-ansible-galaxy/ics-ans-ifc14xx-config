import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ifc14xx_config")


def test_xinted_enabled_and_running(host):
    service = host.service('xinetd')
    assert service.is_enabled
    assert service.is_running


@pytest.mark.parametrize(
    "filepath",
    (
        "/var/lib/tftpboot/ifc14xx_config/uboot_clear_variables.img",
        "/var/lib/tftpboot/ifc14xx_config/uboot_update_env_and_backup.img",
        "/var/lib/tftpboot/ifc14xx_config/uboot_update_uboot.img",
        "/var/lib/tftpboot/ifc14xx_config/uboot_update_pon_fbis.img",
        "/var/lib/tftpboot/u-boot/u-boot-ifc14xx-64b.bin",
        "/var/lib/tftpboot/fbis_pon/pon_microcode_fbis.bin",
    ),
)
def test_files_downloaded(host, filepath):
    assert host.file(filepath).exists
